import * as express from 'express';
// import * as path from 'path';
import * as XLSX from 'xlsx';
import { ErrorHandler } from '../conf/error-handler';
import { CONFIGURATIONS } from '../conf/configurations';

export class MainController {

  // private file = '../../../../files/test-file.csv';
  constructor() { }

  loadFile(req: express.Request, res: express.Response, next: express.NextFunction) {

    let file = '/../../../../files/' + CONFIGURATIONS.localFileName;

    try {

      let workbook = XLSX.readFile(__dirname + file);
      let wsname: string = workbook.SheetNames[0];
      let worksheet: XLSX.WorkSheet = workbook.Sheets[wsname];

      let tempData = XLSX.utils.sheet_to_json(worksheet, { header: 2, blankrows: true, defval: '' });

      res.json(tempData);

    } catch (error) {
      ErrorHandler.send(error, res, next)
    }

  }

  updateFile(req: express.Request, res: express.Response, next: express.NextFunction) {

    let file = '/../../../../files/' + CONFIGURATIONS.localFileName;
    let data = req.body;

    try {

      let worksheet = XLSX.utils.json_to_sheet(data);
      let workbook = { SheetNames: ["Sheet1"], Sheets: { Sheet1: worksheet } };
      XLSX.writeFile(workbook, __dirname + file);


      res.json({ message: true });

    } catch (error) {
      ErrorHandler.send(error, res, next)
    }

  }
}
