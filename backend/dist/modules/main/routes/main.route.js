"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const index_1 = require("../index");
class MainRoute {
    constructor(router) {
        this.router = router;
        this.initRoutes();
    }
    initRoutes() {
        let controller = new index_1.MainController();
        this.router.route('/loadFile').get(controller.loadFile);
        this.router.route('/updateFile').post(controller.updateFile);
    }
}
exports.MainRoute = MainRoute;
//# sourceMappingURL=main.route.js.map