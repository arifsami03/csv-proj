"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CONFIGURATIONS = {
    environment: { frontendURL: '' },
    localFileName: 'sample.csv'
};
exports.DEV_ENV = {
    frontendURL: 'http://localhost:4200'
};
exports.PROD_ENV = {
    frontendURL: ''
};
//# sourceMappingURL=configurations.js.map