"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./routes/main.route"));
__export(require("./controllers/main.controller"));
__export(require("./conf/configurations"));
__export(require("./conf/error-handler"));
//# sourceMappingURL=index.js.map