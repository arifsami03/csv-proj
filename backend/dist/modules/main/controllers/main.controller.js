"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const XLSX = require("xlsx");
const error_handler_1 = require("../conf/error-handler");
const configurations_1 = require("../conf/configurations");
class MainController {
    constructor() { }
    loadFile(req, res, next) {
        let file = '/../../../../files/' + configurations_1.CONFIGURATIONS.localFileName;
        try {
            let workbook = XLSX.readFile(__dirname + file);
            let wsname = workbook.SheetNames[0];
            let worksheet = workbook.Sheets[wsname];
            let tempData = XLSX.utils.sheet_to_json(worksheet, { header: 2, blankrows: true, defval: '' });
            res.json(tempData);
        }
        catch (error) {
            error_handler_1.ErrorHandler.send(error, res, next);
        }
    }
    updateFile(req, res, next) {
        let file = '/../../../../files/' + configurations_1.CONFIGURATIONS.localFileName;
        let data = req.body;
        try {
            let worksheet = XLSX.utils.json_to_sheet(data);
            let workbook = { SheetNames: ["Sheet1"], Sheets: { Sheet1: worksheet } };
            XLSX.writeFile(workbook, __dirname + file);
            res.json({ message: true });
        }
        catch (error) {
            error_handler_1.ErrorHandler.send(error, res, next);
        }
    }
}
exports.MainController = MainController;
//# sourceMappingURL=main.controller.js.map