"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const path = require("path");
const cors = require("cors");
const main_1 = require("./modules/main");
class Server {
    static bootstrap() {
        return new Server();
    }
    constructor() {
        this.app = express();
        this.config();
        this.routes();
    }
    config() {
        main_1.CONFIGURATIONS.environment = process.env.NODE_ENV === 'production' ? main_1.PROD_ENV : main_1.DEV_ENV;
        this.app.use(cors({ origin: main_1.CONFIGURATIONS.environment.frontendURL }));
        this.app.use(express.static(path.join(__dirname, 'public')));
        this.app.use(bodyParser.json({ limit: '250mb' }));
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(cookieParser('SECRET_GOES_HERE'));
        this.app.use(function (err, req, res, next) {
            err.status = 404;
            next(err);
        });
        this.app.use(function (err, req, res, next) {
            var error = new Error('Not Found');
            err.status = 404;
            next(err);
        });
        this.errorHandler();
    }
    errorHandler() {
        this.app.use((err, req, res, next) => {
            console.log('Error handler: -------------------------------------', err);
            return res.status(err.status || 500).send({
                message: err.message || err.name || err
            });
        });
    }
    routes() {
        let router;
        router = express.Router();
        new main_1.MainRoute(router);
        this.app.use('/api/v1/', router);
    }
}
exports.Server = Server;
//# sourceMappingURL=server.js.map