import { Component, OnInit, Renderer2, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, FormControl } from '@angular/forms';

import { MainService } from './../../services/main.service';
import { MainModel } from "./../../models/main-model";

@Component({
  selector: 'app-main-component',
  templateUrl: './main-component.component.html',
  styleUrls: ['./main-component.component.css']
})
export class MainComponentComponent implements OnInit, AfterViewInit {

  loaded: boolean = true;
  goBack: boolean = false;
  successResponse: boolean = false;
  errorResponse: boolean = false;

  fileData: MainModel[];
  isValidCode: boolean = false;
  enteredCode: string = null;

  public codeFG: FormGroup;
  public emailFG: FormGroup;

  public customPatterns = { '0': { pattern: new RegExp('\[a-zA-Z0-9\]') } };

  constructor(
    private fb: FormBuilder,
    private renderer2: Renderer2,
    private mainService: MainService
  ) { }

  ngOnInit() {

    this.codeFG = this.fb.group(new MainModel().codeValidationRule());
    this.emailFG = this.fb.group(new MainModel().emailValidationRule());

  }


  get code(): FormControl { return this.codeFG.get('code') as FormControl; }
  get codeArray(): FormArray { return this.codeFG.get('codeArray') as FormArray; }

  detectFocus(event: any, index: number) {

    if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 65 && event.keyCode <= 90)) {
      if (index !== 4) {
        let elementId = '#control-' + (index + 1);
        this.renderer2.selectRootElement(elementId, true).focus();
      }

      // if code is invalid due to server response reset its value on next key change
      if (this.code.invalid) {
        this.code.reset();
      }
    }
  }

  codeSubmit(data: MainModel) {
    
    let code = data.codeArray.join('');
    this.loaded = false;

    this.mainService.loadFile().subscribe(response => {
      this.fileData = response;

      let isValidCode = this.fileData.find(item => {
        return code.toLowerCase() == String(item.code).toLowerCase();
      });

      if (!isValidCode) {
        this.codeFG.controls.code.setErrors({ notValid: true });
      } else if (isValidCode && isValidCode.redeemed != "") {
        this.codeFG.controls.code.setErrors({ redeemed: true });
      } else {
        this.enteredCode = isValidCode.code;
      }
      this.loaded = true;
    }, error => {
      this.codeFG.controls.code.setErrors({ serverError: true });
      this.loaded = true;
    });

  }

  emailSubmit(data: MainModel) {
    this.loaded = false;

    let elementToUpdate;
    this.fileData.forEach(element => {
      if (element.code == this.enteredCode) {
        element.email = data.email;
        element.redeemed = new Date().getTime();
        elementToUpdate = element;
      }
    });

    this.saveData(elementToUpdate, this.fileData);
  }

  private saveData(element, data) {
    this.mainService.sendDataTo3rdParytApi(element).subscribe(response => {
      this.mainService.updateFile(data).subscribe(response => {
        this.goBack = true;
        this.successResponse = true;
        this.loaded = true;
      }, error => {
        this.goBack = true;
        this.errorResponse = true;
        this.loaded = true;
      });
    }, error => {
      this.goBack = true;
      this.errorResponse = true;
      this.loaded = true;
    });

  }

  private resetPage() {
    this.codeFG.reset();
    this.emailFG.reset();
    this.fileData = null;
    this.enteredCode = null;
    this.goBack = false;
    this.successResponse = false;
    this.errorResponse = false;
  }


  ngAfterViewInit() {
    // focus first element on initialization
    let elementId = '#control-0';
    setTimeout(() => {
      this.renderer2.selectRootElement(elementId, true).focus();
    }, 500);
  }

}
